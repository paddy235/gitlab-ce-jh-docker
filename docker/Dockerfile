
ARG GITLAB_VERSION=17.3.0
## 此镜像来自于 https://repo.openeuler.org/openEuler-24.03-LTS/docker_img/aarch64/
ARG BASE=openeuler/openeuler:24.03-lts
FROM ${BASE}

SHELL ["/bin/sh", "-c"]
ENV LANG=C.UTF-8
## https://docs.gitlab.cn/jh/administration/package_information/supported_os.html
## https://packages.gitlab.cn/#browse/search=keyword%3Daarch64%20AND%20format%3Dyum:c7545579a7a1539017d950a48b58bacb

## 更新版本只需要修改这个版本号
ARG GITLAB_VERSION

## 极狐GitLab
ENV release=jh.0.el9

## 增加支持 aarch64 x86_64
ENV GITLAB_ARCH=aarch64

## 官方地址: https://packages.gitlab.cn/#browse/browse:el:9%2Fgitlab-jh-17.3.0-jh.0.el9.aarch64.rpm
## https://packages.gitlab.cn/repository/el/9/gitlab-jh-17.3.0-jh.0.el9.aarch64.rpm

ENV GITLAB_DOWNLOAD_URL="https://packages.gitlab.cn/repository/el/9/gitlab-jh-${GITLAB_VERSION}-${release}.${GITLAB_ARCH}.rpm"

ENV TZ="Asia/Shanghai"

# Allow to access embedded tools
ENV PATH="/opt/gitlab/embedded/bin:/opt/gitlab/bin:/assets:$PATH"

# Resolve error: TERM environment variable not set.
ENV TERM="xterm"

# Default to supporting utf-8
ENV LANG=C.UTF-8

# Explicitly set supported locales
COPY locale.gen /etc/locale.gen

RUN dnf makecache && \
    ln -s /lib64/libcrypt.so.1.1.0 /lib64/libcrypt.so.2 && \
    dnf install -y ${GITLAB_DOWNLOAD_URL} iproute hostname

    # Copy assets
COPY assets/ /assets/

RUN chmod -R og-w /assets; \
  /assets/setup


# Define data volumes
VOLUME ["/etc/gitlab", "/var/opt/gitlab", "/var/log/gitlab"]

# Expose web & ssh
EXPOSE 443 80 22


##
## docker run --rm -it -v ./gitlab/etc:/etc/gitlab --network host gitlab bash
##
CMD ["/assets/wrapper"]

HEALTHCHECK --interval=60s --timeout=30s --retries=5 \
CMD /opt/gitlab/bin/gitlab-healthcheck --fail --max-time 10
    
