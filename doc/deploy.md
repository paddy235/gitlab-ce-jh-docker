## 部署前提

1. 安装docker环境

2. 已经执行完编译过程

## 部署步骤


### 1. 下载代码

```
git clone https://gitee.com/paddy235/gitlab-ce-jh-docker.git

cd gitlab-ce-jh-docker/docker-compose

```

### 2. 配置参数

配置 [gitlab.rb](/docker-compose/gitlab/etc/gitlab.rb) 你需要的参数，例如:ldap认证


### 3. 启动

`docker-compose up -d`


### 4. 登陆账户密码

账户:root

密码：查看初始密码 `sudo cat gitlab/etc/initial_root_password`

登陆后修改密码.

