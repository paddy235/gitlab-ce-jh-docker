## 1. 主机环境


### 1.1 操作系统

```
[paddy@localhost gitlab]$ hostnamectl
   Static hostname: (unset)                             
Transient hostname: localhost
         Icon name: computer-server
           Chassis: server 🖳
        Machine ID: e06e4536e4d64168b0b02671bea3a57c
           Boot ID: e96ea39a252d46919051a262ed3fb79d
  Operating System: openEuler 24.03 (LTS)
            Kernel: Linux 6.6.0-28.0.0.34.oe2403.aarch64
      Architecture: arm64
   Hardware Vendor: Yunke China
    Hardware Model: KunTai R522
  Firmware Version: 6.69
     Firmware Date: Mon 2024-01-15
      Firmware Age: 7month 6d

```

### 1.2 内存

```
[paddy@localhost gitlab]$ free -g
               total        used        free      shared  buff/cache   available
Mem:             124          38          44           5          47          85
Swap:              3           0           3

```

### 1.3 磁盘

```
[paddy@localhost gitlab]$ df -h
文件系统                    大小  已用  可用 已用% 挂载点
/dev/mapper/openeuler-root   69G   31G   35G   48% /
devtmpfs                    4.0M     0  4.0M    0% /dev
tmpfs                        63G   24K   63G    1% /dev/shm
tmpfs                       4.0M     0  4.0M    0% /sys/fs/cgroup
efivarfs                     83K   41K   42K   50% /sys/firmware/efi/efivars
tmpfs                        25G   96M   25G    1% /run
tmpfs                        63G  243M   63G    1% /tmp
/dev/sdb2                   974M  162M  745M   18% /boot
/dev/sdb1                   599M  6.3M  593M    2% /boot/efi
/dev/mapper/openeuler-home  365G  9.3G  337G    3% /home

```




## docker

```
[paddy@localhost gitlab]$ docker info
Client:
 Version:    27.1.1
 Context:    default
 Debug Mode: false
 Plugins:
  buildx: Docker Buildx (Docker Inc.)
    Version:  v0.16.2
    Path:     /usr/local/libexec/docker/cli-plugins/docker-buildx

Server:
 Containers: 1
  Running: 1
  Paused: 0
  Stopped: 0
 Images: 4
 Server Version: 27.1.1
 Storage Driver: overlay2
  Backing Filesystem: extfs
  Supports d_type: true
  Using metacopy: false
  Native Overlay Diff: true
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: systemd
 Cgroup Version: 1
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local splunk syslog
 Swarm: inactive
 Runtimes: io.containerd.runc.v2 runc
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 8fc6bcff51318944179630522a095cc9dbf9f353
 runc version: v1.1.13-0-g58aa920
 init version: de40ad0
 Security Options:
  seccomp
   Profile: builtin
 Kernel Version: 6.6.0-28.0.0.34.oe2403.aarch64
 Operating System: openEuler 24.03 (LTS)
 OSType: linux
 Architecture: aarch64
 CPUs: 64
 Total Memory: 124.5GiB
 Name: localhost.localdomain
 ID: c3ec6ad0-b48b-4100-b3ff-6a88e7ece6e2
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Experimental: false
 Insecure Registries:
  0.0.0.0/0
  127.0.0.0/8
 Registry Mirrors:
  https://xxx.xxx.xxx.xxx/
 Live Restore Enabled: true
 Product License: Community Engine

```

## docker-compose 

```
[paddy@localhost gitlab]$ docker-compose -v
Docker Compose version v2.29.2

```



