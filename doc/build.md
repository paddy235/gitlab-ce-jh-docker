## 编译

准备基础镜像：`openeuler/openeuler:24.03-lts`

基础镜像也可以在官方地址下载[openEuler-docker.aarch64.tar.xz](https://repo.openeuler.org/openEuler-24.03-LTS/docker_img/aarch64/openEuler-docker.aarch64.tar.xz)


```
git clone https://gitee.com/paddy235/gitlab-ce-jh-docker.git
cd gitlab-ce-jh-docker/docker

docker build --no-cache -t gitlab-ce:jh-17.3.0 .

或者带参数

docker build --no-cache -t gitlab-ce:jh-17.3.0 --build-arg="GITLAB_VERSION=17.3.0" .

```